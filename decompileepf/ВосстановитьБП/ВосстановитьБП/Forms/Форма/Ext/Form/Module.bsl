﻿
&НаКлиенте
Процедура Далее(Команда)	
	Элементы.ТочкаМаршрута.Видимость = Истина;
	Элементы.Задачи.Видимость        = Ложь;
	Элементы.Реквизит1.Видимость     = Ложь;
	Элементы.Просмотр.Видимость      = Ложь;
	Элементы.Группа1.ТекущаяСтраница = Элементы.Группа3; 	
КонецПроцедуры

&НаКлиенте
Процедура ДействиеПриИзменении(Элемент)
	Если Объект.Действие = "НачатьЗадачи" Тогда
		Элементы.ТочкаМаршрута.Видимость = Истина;
		Элементы.Задачи.Видимость        = Ложь;
		Элементы.Реквизит1.Видимость     = Ложь;
		Элементы.Просмотр.Видимость      = Ложь;
	Иначе
		Элементы.ТочкаМаршрута.Видимость = Ложь;
		Элементы.Задачи.Видимость        = Истина;
		Элементы.Реквизит1.Видимость     = Истина;
		Элементы.Просмотр.Видимость      = Истина;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ВыполнитьДействие(Команда)
	     	
	Если Объект.Действие = "НачатьЗадачи" Тогда 		
		Если ЗначениеЗаполнено(Объект.БизнесПроцесс) И ЗначениеЗаполнено(Объект.ТочкаМаршрута) И ЗначениеЗаполнено(Объект.Действие) Тогда
			ВыполнитьДействиеНаСервере1();
		Иначе
			Сообщить("Заполните все обязательные поля");
		КонецЕсли;			
	Иначе
		Реквизит1.Очистить();
		ПросмотрНаСервере();
		Режим      = РежимДиалогаВопрос.ДаНет;
		Оповещение = Новый ОписаниеОповещения("ПослеЗакрытияВопроса", ЭтотОбъект, Параметры);
		ПоказатьВопрос(Оповещение, НСтр("ru = 'Вы уверены, что хотите продолжить?';"), Режим, 0);				
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗакрытияВопроса(Результат, Параметры) Экспорт 
		
	Если Результат = КодВозвратаДиалога.Нет Тогда
		Возврат;
	КонецЕсли;
		
	Если ЗначениеЗаполнено(Объект.Задачи) И ЗначениеЗаполнено(Объект.БизнесПроцесс) И ЗначениеЗаполнено(Объект.Действие) Тогда
		ВыполнитьДействиеНаСервере2();
	Иначе
		Сообщить("Заполните все обязательные поля");
	КонецЕсли; 	
	
КонецПроцедуры

&НаСервере
Процедура ВыполнитьДействиеНаСервере1()
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ РАЗРЕШЕННЫЕ РАЗЛИЧНЫЕ
		|	ДомРФ_ФинансированиеМероприятий.Ссылка КАК Ссылка
		|ИЗ
		|	Задача.ДомРФ_ФинансированиеМероприятий КАК ДомРФ_ФинансированиеМероприятий
		|ГДЕ
		|	ДомРФ_ФинансированиеМероприятий.БизнесПроцесс = &БизнесПроцесс
		|	И ДомРФ_ФинансированиеМероприятий.ТочкаМаршрута = &ТочкаМаршрута";
	
	Запрос.УстановитьПараметр("БизнесПроцесс", Объект.БизнесПроцесс);
	Запрос.УстановитьПараметр("ТочкаМаршрута", Объект.ТочкаМаршрута);
	
	Выборка = Запрос.Выполнить().Выбрать();	
	
	ТаблицаВсехЗадач = ДомРФ_БизнесПроцессыИЗадачиВызовСервера.ПолучитьСписокВсехЗадачБизнесПроцесса(Объект.БизнесПроцесс);
	
	Если Выборка.Количество() = 0 Тогда		
		Если СтрНайти(СокрЛП(Объект.ТочкаМаршрута),"Подпроцесс",НаправлениеПоиска.СНачала) > 0 Тогда
			
			//создадим задачу подпроцесса и сразу же её закроем
			ЗадачаОбъект = Задачи.ДомРФ_ФинансированиеМероприятий.СоздатьЗадачу();
			ЗадачаОбъект.Автор =  ?(ТаблицаВсехЗадач.Количество() > 0,ТаблицаВсехЗадач[ТаблицаВсехЗадач.Количество()-1].Ссылка.Автор,Пользователи.ТекущийПользователь());
			ЗадачаОбъект.БизнесПроцесс  = Объект.БизнесПроцесс;
			ЗадачаОбъект.Выполнена = Ложь;
			ЗадачаОбъект.Дата = ТекущаяДата();
			ЗадачаОбъект.Наименование  = СокрЛП(Объект.ТочкаМаршрута);
			ЗадачаОбъект.Исполнитель  = Пользователи.АвторизованныйПользователь();
			ЗадачаОбъект.СостояниеБизнесПроцесса = Перечисления.СостоянияБизнесПроцессов.Активен;
			ЗадачаОбъект.СрокИсполненияПлан  = ТекущаяДата();
			ЗадачаОбъект.Статус   = Перечисления.ДомРФ_СтатусыЗадачФинансированиеМероприятий.Новая;
			ЗадачаОбъект.ТочкаМаршрута  = Объект.ТочкаМаршрута;
			ЗадачаОбъект.Записать();			
			
			// создадим вложенный бизнес-процесс и стартанём его
			
			БПОбъект = Объект.ТочкаМаршрута.ВложенныйПроцесс.СоздатьБизнесПроцесс();
			БПОбъект.Автор = Пользователи.ТекущийПользователь();
			БПОбъект.ВедущаяЗадача = ЗадачаОбъект.Ссылка;
			БПОбъект.Дата = ТекущаяДата();
			БПОбъект.Записать();
			БПОбъект.Старт(Объект.ТочкаМаршрута.ВложенныйПроцесс.ТочкиМаршрута.Старт);
			БПОбъект.Записать();
			
			//ЗадачаОбъект = Задачи.ДомРФ_ФинансированиеМероприятий.СоздатьЗадачу();
			//ЗадачаОбъект.Автор =  ?(ТаблицаВсехЗадач.Количество() > 0,ТаблицаВсехЗадач[ТаблицаВсехЗадач.Количество()-1].Ссылка.Автор,Пользователи.ТекущийПользователь());
			//ЗадачаОбъект.БизнесПроцесс  = БПОбъект.Ссылка;
			//ЗадачаОбъект.Выполнена = Ложь;
			//ЗадачаОбъект.Дата = ТекущаяДата();
			//ЗадачаОбъект.Наименование  = СокрЛП(Объект.ТочкаМаршрута.ВложенныйПроцесс.ТочкиМаршрута.ЗаказатьОтчетОбОценке);
			//ЗадачаОбъект.РольИсполнителя  = Объект.ТочкаМаршрута.ВложенныйПроцесс.ТочкиМаршрута.ЗаказатьОтчетОбОценке.РольИсполнителя;
			//ЗадачаОбъект.СостояниеБизнесПроцесса = Перечисления.СостоянияБизнесПроцессов.Активен;
			//ЗадачаОбъект.СрокИсполненияПлан  = ДомРФ_ФинансированиеМероприятий_Функционал.ПолучитьПлановыйСрокИсполненияЗадачи(Объект.ТочкаМаршрута.ВложенныйПроцесс.ТочкиМаршрута.ЗаказатьОтчетОбОценке,ТекущаяДата());
			//ЗадачаОбъект.Статус   = Перечисления.ДомРФ_СтатусыЗадачФинансированиеМероприятий.Новая;
			//ЗадачаОбъект.ТочкаМаршрута  = Объект.ТочкаМаршрута.ВложенныйПроцесс.ТочкиМаршрута.ЗаказатьОтчетОбОценке;
			//ЗадачаОбъект.Записать();
			
			
			//создадим первую задачу вложенного бизнес-процесса
			
			
			
		Иначе				
			ЗадачаОбъект = Задачи.ДомРФ_ФинансированиеМероприятий.СоздатьЗадачу();
			ЗадачаОбъект.Автор =  ?(ТаблицаВсехЗадач.Количество() > 0,ТаблицаВсехЗадач[ТаблицаВсехЗадач.Количество()-1].Ссылка.Автор,Пользователи.ТекущийПользователь());
			ЗадачаОбъект.БизнесПроцесс  = Объект.БизнесПроцесс;
			ЗадачаОбъект.Выполнена = Ложь;
			ЗадачаОбъект.Дата = ТекущаяДата();
			ЗадачаОбъект.Наименование  = СокрЛП(Объект.ТочкаМаршрута);
			ЗадачаОбъект.РольИсполнителя  = Объект.ТочкаМаршрута.РольИсполнителя;
			ЗадачаОбъект.СостояниеБизнесПроцесса = Перечисления.СостоянияБизнесПроцессов.Активен;
			ЗадачаОбъект.СрокИсполненияПлан  = ДомРФ_ФинансированиеМероприятий_Функционал.ПолучитьПлановыйСрокИсполненияЗадачи(Объект.ТочкаМаршрута,ТекущаяДата());
			ЗадачаОбъект.Статус   = Перечисления.ДомРФ_СтатусыЗадачФинансированиеМероприятий.Новая;
			ЗадачаОбъект.ТочкаМаршрута  = Объект.ТочкаМаршрута;
			ЗадачаОбъект.Записать();			
		КонецЕсли;	
		
	Иначе
		Выборка.Следующий();		
		БПОбъект = Объект.ТочкаМаршрута.ВложенныйПроцесс.СоздатьБизнесПроцесс();
		БПОбъект.Автор = Пользователи.ТекущийПользователь();
		БПОбъект.ВедущаяЗадача = Выборка.Ссылка;
		БПОбъект.Дата = ТекущаяДата();
		БПОбъект.ОбменДанными.Загрузка = Истина;
		БПОбъект.Стартован = Истина;
		БПОбъект.Записать();
		//БПОбъект.Старт(Объект.ТочкаМаршрута.ВложенныйПроцесс.ТочкиМаршрута.Старт);
		//БПОбъект.Записать();
		
		//ЗадачаОбъект = Задачи.ДомРФ_ФинансированиеМероприятий.СоздатьЗадачу();
		//ЗадачаОбъект.Автор =  ?(ТаблицаВсехЗадач.Количество() > 0,ТаблицаВсехЗадач[ТаблицаВсехЗадач.Количество()-1].Ссылка.Автор,Пользователи.ТекущийПользователь());
		//ЗадачаОбъект.БизнесПроцесс  = БПОбъект.Ссылка;
		//ЗадачаОбъект.Выполнена = Ложь;
		//ЗадачаОбъект.Дата = ТекущаяДата();
		//ЗадачаОбъект.Наименование  = СокрЛП(Объект.ТочкаМаршрута.ВложенныйПроцесс.ТочкиМаршрута.ЗаказатьОтчетОбОценке);
		//ЗадачаОбъект.РольИсполнителя  = Объект.ТочкаМаршрута.ВложенныйПроцесс.ТочкиМаршрута.ЗаказатьОтчетОбОценке.РольИсполнителя;
		//ЗадачаОбъект.СостояниеБизнесПроцесса = Перечисления.СостоянияБизнесПроцессов.Активен;
		//ЗадачаОбъект.СрокИсполненияПлан  = ДомРФ_ФинансированиеМероприятий_Функционал.ПолучитьПлановыйСрокИсполненияЗадачи(Объект.ТочкаМаршрута.ВложенныйПроцесс.ТочкиМаршрута.ЗаказатьОтчетОбОценке,ТекущаяДата());
		//ЗадачаОбъект.Статус   = Перечисления.ДомРФ_СтатусыЗадачФинансированиеМероприятий.Новая;
		//ЗадачаОбъект.ТочкаМаршрута  = Объект.ТочкаМаршрута.ВложенныйПроцесс.ТочкиМаршрута.ЗаказатьОтчетОбОценке;
		//ЗадачаОбъект.Записать();	
	КонецЕсли;	

КонецПроцедуры

//+ДОМ.РФ: A.Zemlyanov 26.05.2021 MZ-2919
&НаСервере
Процедура ВыполнитьДействиеНаСервере2()	
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	ДомРФ_ФинансированиеМероприятий.Номер КАК Номер,
		|	ДомРФ_ФинансированиеМероприятий.Ссылка КАК Ссылка
		|ИЗ
		|	Задача.ДомРФ_ФинансированиеМероприятий КАК ДомРФ_ФинансированиеМероприятий
		|ГДЕ
		|	(ДомРФ_ФинансированиеМероприятий.БизнесПроцесс = &БизнесПроцесс
		|			ИЛИ ДомРФ_ФинансированиеМероприятий.БизнесПроцесс.ВедущаяЗадача.БизнесПроцесс = &БизнесПроцесс)
		|	И ДомРФ_ФинансированиеМероприятий.Дата > ВЫРАЗИТЬ(&МояЗадача КАК Задача.ДомРФ_ФинансированиеМероприятий).Дата";
	
	Запрос.УстановитьПараметр("БизнесПроцесс", Объект.БизнесПроцесс);
	Запрос.УстановитьПараметр("МояЗадача", Объект.Задачи);
		
	Выборка = Запрос.Выполнить().Выбрать();	
		
	Пока Выборка.Следующий() Цикл 	       					
			БПОбъект = Выборка.Ссылка.ПолучитьОбъект();
			БПОбъект.Удалить();
	КонецЦикла;
		
	ОткатЗадачи                    = Объект.Задачи.ПолучитьОбъект();
	ОткатЗадачи.Выполнена          = Ложь;	
	ОткатЗадачи.СрокИсполненияФакт = Дата(1, 1, 1);
	ОткатЗадачи.Статус             = Перечисления.ДомРФ_СтатусыЗадачФинансированиеМероприятий.ВРаботе;
	ОткатЗадачи.Записать();	
	
КонецПроцедуры

&НаКлиенте
Процедура Просмотр(Команда)
	Реквизит1.Очистить();
	ПросмотрНаСервере();
КонецПроцедуры

&НаСервере
Процедура ПросмотрНаСервере()
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	ДомРФ_ФинансированиеМероприятий.Номер КАК Номер,
		|	ДомРФ_ФинансированиеМероприятий.Ссылка КАК Ссылка
		|ИЗ
		|	Задача.ДомРФ_ФинансированиеМероприятий КАК ДомРФ_ФинансированиеМероприятий
		|ГДЕ
		|	(ДомРФ_ФинансированиеМероприятий.БизнесПроцесс = &БизнесПроцесс
		|			ИЛИ ДомРФ_ФинансированиеМероприятий.БизнесПроцесс.ВедущаяЗадача.БизнесПроцесс = &БизнесПроцесс)
		|	И ДомРФ_ФинансированиеМероприятий.Дата > ВЫРАЗИТЬ(&МояЗадача КАК Задача.ДомРФ_ФинансированиеМероприятий).Дата";
	
	Запрос.УстановитьПараметр("БизнесПроцесс", Объект.БизнесПроцесс);
	Запрос.УстановитьПараметр("МояЗадача", Объект.Задачи);
		
	Выборка = Запрос.Выполнить().Выбрать();	
	
	Сч = 1;
	Пока Выборка.Следующий() Цикл
		НСТр        = Реквизит1.Добавить();
		НСТр.Ном    = Сч;
		НСТр.Номер  = Выборка.Номер;
		НСТр.Ссылка = Выборка.Ссылка;
		Сч          = Сч + 1; 
	КонецЦикла;
	
КонецПроцедуры
//-ДОМ.РФ: A.Zemlyanov 26.05.2021 

